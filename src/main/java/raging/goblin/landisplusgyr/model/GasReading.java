package raging.goblin.landisplusgyr.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GasReading {

    private LocalDateTime dateTime;
    private Double reading;

    public boolean isComplete() {
        return dateTime != null && reading != null;
    }
}
