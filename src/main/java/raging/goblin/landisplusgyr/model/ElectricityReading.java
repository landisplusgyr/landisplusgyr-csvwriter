package raging.goblin.landisplusgyr.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ElectricityReading {

    private Double reading;

    public boolean isComplete() {
        return reading != null;
    }
}
