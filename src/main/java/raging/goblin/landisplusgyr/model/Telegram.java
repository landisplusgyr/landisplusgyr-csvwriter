package raging.goblin.landisplusgyr.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Telegram {

    private ElectricityReading electricityReceivedTariff1;
    private ElectricityReading electricityReceivedTariff2;
    private ElectricityReading electricityDeliveredTariff1;
    private ElectricityReading electricityDeliveredTariff2;
    private GasReading gasReading;
    private LocalDateTime dateTime;

    public boolean isComplete() {
        return gasReading != null
                && electricityReceivedTariff1 != null
                && electricityReceivedTariff2 != null
                && electricityDeliveredTariff1 != null
                && electricityDeliveredTariff2 != null
                && dateTime != null;
    }

    public boolean isElectricityComplete() {
        return electricityReceivedTariff1 != null && electricityReceivedTariff1.isComplete()
                && electricityReceivedTariff2 != null && electricityReceivedTariff2.isComplete()
                && electricityDeliveredTariff1 != null && electricityDeliveredTariff1.isComplete()
                && electricityDeliveredTariff2 != null && electricityDeliveredTariff2.isComplete()
                && dateTime != null;
    }

    public boolean isGasComplete() {
        return gasReading != null && gasReading.isComplete();
    }
}
