package raging.goblin.landisplusgyr.service;

import static java.lang.Boolean.parseBoolean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import purejavacomm.CommPortIdentifier;
import purejavacomm.NoSuchPortException;
import purejavacomm.PortInUseException;
import purejavacomm.SerialPort;
import purejavacomm.UnsupportedCommOperationException;
import raging.goblin.landisplusgyr.model.ElectricityReading;
import raging.goblin.landisplusgyr.model.GasReading;
import raging.goblin.landisplusgyr.model.Telegram;

@Slf4j
@Service
public class SerialPortReader {

    @Value("${raging.goblin.landisplusgyr.reading.enabled}")
    private String readingEnabled;
    @Value("${raging.goblin.landisplusgyr.gasreading.enabled}")
    private String gasReadingEnabled;
    @Value("${raging.goblin.landisplusgyr.electricityreading.enabled}")
    private String electricityReadingEnabled;

    @Value("${raging.goblin.landisplusgyr.telegram.datetime.line.format}")
    private String dateTimeLineFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.datetime.format}")
    private String dateTimeFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.datetime.dateformatter.format}")
    private String dateTimeDateFormattterFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.electricity.received.format}")
    private String electricityFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.electricity.received.tariff1.line.format}")
    private String electricityReceivedTariff1Format;
    @Value("${raging.goblin.landisplusgyr.telegram.electricity.received.tariff2.line.format}")
    private String electricityReceivedTariff2Format;
    @Value("${raging.goblin.landisplusgyr.telegram.electricity.delivered.tariff1.line.format}")
    private String electricityDeliveredTariff1Format;
    @Value("${raging.goblin.landisplusgyr.telegram.electricity.delivered.tariff2.line.format}")
    private String electricityDeliveredTariff2Format;
    @Value("${raging.goblin.landisplusgyr.telegram.gas.received.line.format}")
    private String gasLineFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.gas.received.format}")
    private String gasFormat;
    @Value("${raging.goblin.landisplusgyr.telegram.end.line.format}")
    private String endLineFormat;

    @Value("${raging.goblin.landisplusgyr.reading.delay}")
    private String readingDelay;
    @Value("${raging.goblin.landisplusgyr.serial.port}")
    private String serialPort;
    @Value("${raging.goblin.landisplusgyr.serial.port.baudRate}")
    private String baudRate;
    @Value("${raging.goblin.landisplusgyr.serial.port.dataBits}")
    private String dataBits;
    @Value("${raging.goblin.landisplusgyr.serial.port.stopBits}")
    private String stopBits;
    @Value("${raging.goblin.landisplusgyr.serial.port.parity}")
    private String parity;

    @Autowired
    private ThreadPoolTaskScheduler scheduler;
    @Autowired
    private CSVWriter csvWriter;

    private Telegram telegram;
    private GasReading lastGasReveived;
    private LocalDateTime lastElectricityReceived;

    private DateTimeFormatter dateTimeFormatter;
    private Pattern dateTimePattern;
    private Pattern gasPattern;
    private Pattern electricityPattern;

    private BufferedReader portReader;
    private SerialPort port;

    private boolean keepReading;

    @PostConstruct
    private void initSerialPortReader() {
        if (Boolean.valueOf(readingEnabled)) {
            log.info("Initialize serial port reader");
            initLastReading();
            initPatterns();
            initSerialPortReading();
        } else {
            log.info("Reading disabled, serial port reader will not start");
        }
    }

    @PreDestroy
    private void stopReading() {
        log.info("Stop serial port reader");
        scheduler.shutdown();
        keepReading = false;
        closeSerialPort();
    }

    private void initLastReading() {
        this.lastGasReveived = GasReading.builder().reading(0.0).dateTime(LocalDateTime.now()).build();
        this.lastElectricityReceived = LocalDateTime.now();
    }

    private void initPatterns() {
        dateTimePattern = Pattern.compile(dateTimeFormat);
        dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeDateFormattterFormat);
        gasPattern = Pattern.compile(gasFormat);
        electricityPattern = Pattern.compile(electricityFormat);
    }

    private void initSerialPortReading() {
        log.debug("Initialize reading from port {} at an interval of {}s", serialPort, readingDelay);

        keepReading = true;

        Runnable readingThread = () -> {
            log.info("Started reading from serial port");
            try {
                initializePort();
                readTelegram();
                closeSerialPort();
            } catch (NoSuchPortException | PortInUseException | UnsupportedCommOperationException | IOException e) {
                log.error("Not able to open serial port for reading {}", serialPort, e);
            }
        };
        scheduler.scheduleWithFixedDelay(readingThread, Long.parseLong(readingDelay) * 1000);
    }

    private void closeSerialPort() {
        try {
            if (portReader != null) {
                portReader.close();
            }
            if (port != null) {
                port.close();
            }
        } catch (IOException e) {
            log.error("Unable to close serial port");
        }
    }

    private void initializePort() throws PortInUseException, NoSuchPortException, NumberFormatException,
            UnsupportedCommOperationException, IOException {

        log.debug("Initializing serial port {}, {} {} {}", serialPort, baudRate, dataBits, stopBits);

        CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(serialPort);
        port = (SerialPort) portId.open("raging.goblin.landisplusgyr", 1000);
        port.setSerialPortParams(Integer.parseInt(baudRate), Integer.parseInt(dataBits), Integer.parseInt(stopBits),
                Integer.parseInt(parity));
        InputStream inStream = port.getInputStream();
        portReader = new BufferedReader(new InputStreamReader(inStream));

        log.debug("Serial port initialized {}, {} {} {}", serialPort, baudRate, dataBits, stopBits);
    }

    private void readTelegram() {
        telegram = new Telegram();
        while (keepReading && !isTelegramComplete()) {
            try {
                String line = portReader.readLine();
                log.trace(line);
                parseReadLine(line);
            } catch (IOException e) {
                log.error("Error reading line from serial port", e);
            }
        }
    }

    private void parseReadLine(String line) {
        try {
            if (isEndLine(line)) {
                telegram = new Telegram();
            }
            if (isDateTimeLine(line)) {
                readDateTimeToTelegram(line);
            }
            if (isGasReadingLine(line)) {
                readGasReceivedToTelegram(line);
            }
            if (isElectricityReceivedTariff1Line(line)) {
                readElectricityReading(line).ifPresent(er -> {
                    telegram.setElectricityReceivedTariff1(er);
                    log.trace("Electricity reading read received tariff 1: {}", telegram.getElectricityReceivedTariff1());
                });
            }
            if (isElectricityReceivedTariff2Line(line)) {
                readElectricityReading(line).ifPresent(er -> {
                    telegram.setElectricityReceivedTariff2(er);
                    log.trace("Electricity reading read received tariff 2: {}", telegram.getElectricityReceivedTariff2());
                });
            }
            if (isElectricityDeliveredTariff1Line(line)) {
                readElectricityReading(line).ifPresent(er -> {
                    telegram.setElectricityDeliveredTariff1(er);
                    log.trace("Electricity reading read delivered tariff 1: {}", telegram.getElectricityDeliveredTariff1());
                });
            }
            if (isElectricityDeliveredTariff2Line(line)) {
                readElectricityReading(line).ifPresent(er -> {
                    telegram.setElectricityDeliveredTariff2(er);
                    log.trace("Electricity reading read delivered tariff 2: {}", telegram.getElectricityDeliveredTariff2());
                });
            }
        } catch (RuntimeException e) {
            log.error("Unable to parse line correctly: {}", line, e);
        }

        if (isTelegramComplete()) {
            log.debug("Telegram is complete: {}", telegram);
            storeTelegram();
        }
    }

    private void readDateTimeToTelegram(final String line) {
        Matcher dateTimeMatcher = dateTimePattern.matcher(line);
        if (dateTimeMatcher.find()) {
            LocalDateTime dateTime = LocalDateTime.parse(dateTimeMatcher.group(), dateTimeFormatter);
            telegram.setDateTime(dateTime);
            log.trace("Datetime read: {}", telegram.getDateTime());
        }
    }

    private void readGasReceivedToTelegram(final String line) {
        Matcher gasMatcher = gasPattern.matcher(line);
        Matcher dateTimeMatcher = dateTimePattern.matcher(line);
        if (gasMatcher.find() && dateTimeMatcher.find()) {
            double gasReading = Double.parseDouble(gasMatcher.group());
            LocalDateTime dateTime = LocalDateTime.parse(dateTimeMatcher.group(), dateTimeFormatter);
            telegram.setGasReading(GasReading.builder().dateTime(dateTime).reading(gasReading).build());
            log.trace("Gas reading read: {}", telegram.getGasReading());
        }
    }

    private Optional<ElectricityReading> readElectricityReading(final String line) {
        Matcher electricityMatcher = electricityPattern.matcher(line);
        if (electricityMatcher.find()) {
            double electricityReading = Double.parseDouble(electricityMatcher.group());
            return Optional.of(ElectricityReading.builder().reading(electricityReading).build());
        }
        return Optional.empty();
    }

    private void storeTelegram() {
        if (telegram.isGasComplete() && isLongerThanDelay(lastGasReveived.getDateTime(), telegram.getGasReading().getDateTime())) {
            lastGasReveived = telegram.getGasReading();
            csvWriter.writeGasValue(telegram);
        }

        if (telegram.isElectricityComplete() && isLongerThanDelay(lastElectricityReceived, telegram.getDateTime())) {
            lastElectricityReceived = telegram.getDateTime();
            csvWriter.writeElectricityValues(telegram);
        }
    }

    private boolean isTelegramComplete() {
        return telegram.isComplete()
                || (parseBoolean(gasReadingEnabled) && !parseBoolean(electricityReadingEnabled) && telegram.isGasComplete())
                || (!parseBoolean(gasReadingEnabled) && parseBoolean(electricityReadingEnabled) && telegram.isElectricityComplete());
    }

    private boolean isElectricityReceivedTariff1Line(String line) {
        return line.matches(electricityReceivedTariff1Format);
    }

    private boolean isElectricityReceivedTariff2Line(String line) {
        return line.matches(electricityReceivedTariff2Format);
    }

    private boolean isElectricityDeliveredTariff1Line(String line) {
        return line.matches(electricityDeliveredTariff1Format);
    }

    private boolean isElectricityDeliveredTariff2Line(String line) {
        return line.matches(electricityDeliveredTariff2Format);
    }

    private boolean isGasReadingLine(String line) {
        return line.matches(gasLineFormat);
    }

    private boolean isDateTimeLine(String line) {
        return line.matches(dateTimeLineFormat);
    }

    private boolean isEndLine(String line) {
        return endLineFormat.equals(line);
    }

    private boolean isLongerThanDelay(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end).getSeconds() >= Long.parseLong(readingDelay);
    }
}
