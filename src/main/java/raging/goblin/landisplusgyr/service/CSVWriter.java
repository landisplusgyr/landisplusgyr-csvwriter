package raging.goblin.landisplusgyr.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import raging.goblin.landisplusgyr.model.GasReading;
import raging.goblin.landisplusgyr.model.Telegram;

@Slf4j
@Service
public class CSVWriter {

    @Value("${raging.goblin.landisplusgyr.gasvalues.file}")
    private String gasFileName;
    @Value("${raging.goblin.landisplusgyr.gasvalues.fileheader}")
    private String gasFileHeader;
    @Value("${raging.goblin.landisplusgyr.electricityvalues.file}")
    private String electricityFileName;
    @Value("${raging.goblin.landisplusgyr.electricityvalues.fileheader}")
    private String electricityFileHeader;

    public void writeElectricityValues(Telegram telegram) {
        createFileIfNotExists(electricityFileName, electricityFileHeader);
        final String electricityLine = String.format("%s;%f;%f;%f;%f",
                telegram.getDateTime().toString(),
                telegram.getElectricityReceivedTariff1().getReading(),
                telegram.getElectricityReceivedTariff2().getReading(),
                telegram.getElectricityDeliveredTariff1().getReading(),
                telegram.getElectricityDeliveredTariff2().getReading());
        appendReading(electricityFileName, electricityLine);
        log.debug("ElectricityReading of {} {} {} {} stored at {}",
                telegram.getElectricityReceivedTariff1().getReading(),
                telegram.getElectricityReceivedTariff2().getReading(),
                telegram.getElectricityDeliveredTariff1().getReading(),
                telegram.getElectricityDeliveredTariff2().getReading(),
                telegram.getDateTime());
    }

    public void writeGasValue(Telegram telegram) {
        createFileIfNotExists(gasFileName, gasFileHeader);
        final GasReading gasReading = telegram.getGasReading();
        final String gasLine = String.format("%s;%f", gasReading.getDateTime().toString(), gasReading.getReading());
        appendReading(gasFileName, gasLine);
        log.debug("Gasreading of {} stored at {}", gasReading.getReading(), gasReading.getDateTime());
    }

    private void appendReading(String fileName, String lineToAppend) {
        Path path = Paths.get(fileName);
        try {
            Files.writeString(path, lineToAppend + System.lineSeparator(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            log.error("Unable to write line '{}' to file {}", lineToAppend, fileName, e);
        }
    }

    private void createFileIfNotExists(String fileName, final String fileHeader) {
        try {
            File file = new File(fileName);
            log.trace("Initializing CSV file at {}", file.getAbsolutePath());
            if (!file.exists()) {
                log.debug("Creating CSV file at {}", file.getAbsolutePath());
                Files.createFile(file.toPath());
                Files.writeString(file.toPath(), fileHeader + System.lineSeparator(), StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            log.error("Not able to create file {}", fileName, e);
        }
    }
}
